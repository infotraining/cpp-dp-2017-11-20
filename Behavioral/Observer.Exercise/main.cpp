#include "stock.hpp"

using namespace std;

int main()
{
    Stock misys("Misys", 340.0);
    Stock ibm("IBM", 245.0);
    Stock tpsa("TPSA", 95.0);

    // rejestracja inwestorow zainteresowanych powiadomieniami o zmianach kursu spolek
    auto kulczyk_jr = make_shared<Investor>("Kulczyk Jr");
    auto solorz = make_shared<Investor>("Solorz");

    ibm.attach(kulczyk_jr);
    misys.attach(solorz);
    ibm.attach(solorz);

    // zmian kursow
    misys.set_price(360.0);
    ibm.set_price(210.0);
    tpsa.set_price(45.0);

    cout << "\n\n";

    solorz.reset(); //

    misys.set_price(380.0);
    ibm.set_price(230.0);
    tpsa.set_price(15.0);
}
