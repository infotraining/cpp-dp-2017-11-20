#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <iostream>
#include <string>
#include <memory>
#include <set>

class Observer
{
public:
    virtual void update(std::string symbol, double price) = 0;
    virtual ~Observer()
    {
    }
};

// Subject
class Stock
{
private:
    std::string symbol_;
    double price_;

    using ObserverWPtr = std::weak_ptr<Observer>;
    std::set<ObserverWPtr, std::owner_less<ObserverWPtr>> observers_;
public:
    Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
    {
    }

    std::string get_symbol() const
    {
        return symbol_;
    }

    double get_price() const
    {
        return price_;
    }

    // TODO: rejestracja obserwatora
    void attach(ObserverWPtr o)
    {
        observers_.insert(o);
    }

    // TODO: wyrejestrowanie obserwatora
    void detach(ObserverWPtr o)
    {
        observers_.erase(o);
    }

    void set_price(double price)
    {
        if (price_ != price)
        {
            price_ = price;
            notify();
        }
        // TODO: powiadomienie inwestorow o zmianie kursu...
    }
protected:
    void notify()
    {
        auto it = observers_.begin();

        while(it != observers_.end())
        {
            std::shared_ptr<Observer> living_observer = it->lock();
            if (living_observer)
            {
                living_observer->update(symbol_, price_);
                ++it;
            }
            else
                it = observers_.erase(it);
        }
    }
};

class Investor : public Observer
{
    std::string name_;

public:
    Investor(const std::string& name) : name_(name)
    {
    }

    void update(std::string symbol, double price) override
    {
        std::cout << name_ << " is notified: " << symbol << " - " << price << "$" << std::endl;
    }
};

#endif /*STOCK_HPP_*/
