#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>

namespace Bank
{
    class InsufficientFunds : public std::runtime_error
    {
        const int id_;

    public:
        InsufficientFunds(const std::string& msg, int id) : std::runtime_error{msg}, id_{id}
        {
        }

        int id() const
        {
            return id_;
        }
    };

    enum AccountState
    {
        overdraft,
        normal
    };

    class BankAccount
    {
        int id_;
        double balance_;

        class AccountState
        {
        public:
            virtual void withdraw(double amount, BankAccount& ba) const = 0;
            virtual void pay_intest(BankAccount& ba) const = 0;
            virtual std::string status() const = 0;
            virtual ~AccountState() = default;
        };

        class NormalState : public AccountState
        {
            static double constexpr interest_rate = 0.05;
        public:
            void withdraw(double amount, BankAccount& ba) const;
            void pay_intest(BankAccount& ba) const;
            std::string status() const;
        };

        class OverdraftState : public AccountState
        {
            static double constexpr interest_rate = 0.15;
        public:
            void withdraw(double amount, BankAccount& ba) const;
            void pay_intest(BankAccount& ba) const;
            std::string status() const;
        };

        static const NormalState normal_state;
        static const OverdraftState overdraft_state;

        const AccountState* state_;

    protected:
        void update_account_state()
        {                       
            if (balance_ < 0)
                state_ = &overdraft_state;
            else
                state_ = &normal_state;
        }

        void set_balance(double amount)
        {
            balance_ = amount;
        }

    public:
        BankAccount(int id) : id_(id), balance_(0.0), state_(&normal_state) {}

        void withdraw(double amount)
        {
            assert(amount > 0);
            state_->withdraw(amount, *this);

            update_account_state();
        }

        void deposit(double amount)
        {
            assert(amount > 0);
            balance_ += amount;

            update_account_state();
        }

        void pay_interest()
        {
            state_->pay_intest(*this);
        }

        std::string status() const
        {
            std::stringstream strm;
            strm << "BankAccount #" << id_ << "; State: ";
            strm << state_->status() << "; ";
            strm << "Balance: " << std::to_string(balance_);

            return strm.str();
        }

        double balance() const
        {
            return balance_;
        }

        int id() const
        {
            return id_;
        }
    };
}

#endif
