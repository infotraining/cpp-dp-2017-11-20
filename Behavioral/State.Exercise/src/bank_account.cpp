#include "bank_account.hpp"

namespace Bank
{
    const BankAccount::NormalState BankAccount::normal_state;
    const BankAccount::OverdraftState BankAccount::overdraft_state;

    void BankAccount::NormalState::withdraw(double amount, BankAccount& ba) const
    {
        ba.balance_ -= amount;
    }

    void BankAccount::NormalState::pay_intest(BankAccount& ba) const
    {
        ba.balance_ += ba.balance_ * NormalState::interest_rate;
    }

    std::string BankAccount::NormalState::status() const
    {
        return "normal";
    }

    void BankAccount::OverdraftState::withdraw(double amount, BankAccount& ba) const
    {
        throw InsufficientFunds{"Insufficient funds for account #" + std::to_string(ba.id_), ba.id_};
    }

    void BankAccount::OverdraftState::pay_intest(BankAccount& ba) const
    {
        ba.balance_ += ba.balance_ * OverdraftState::interest_rate;
    }

    std::string BankAccount::OverdraftState::status() const
    {
        return "overdraft";
    }
}
