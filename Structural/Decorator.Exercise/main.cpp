#include "starbugs_coffee.hpp"
#include <memory>

class CoffeeBuilder
{
    std::unique_ptr<Coffee> coffee_;
public:
    template <typename BaseT>
    CoffeeBuilder& create_base()
    {
        coffee_ = std::make_unique<BaseT>();

        return *this;
    }

    template <typename CondimentT>
    CoffeeBuilder& add()
    {
        coffee_ = std::make_unique<CondimentT>(std::move(coffee_));

        return *this;
    }

    std::unique_ptr<Coffee> get_coffee()
    {
        return std::move(coffee_);
    }
};

void client(std::unique_ptr<Coffee> coffee)
{
    std::cout << "Description: " << coffee->get_description() << "; Price: " << coffee->get_total_price() << std::endl;
    coffee->prepare();
}

int main()
{
    std::unique_ptr<Coffee> cf =
            std::make_unique<Whipped>(
                std::make_unique<Whisky>(
                    std::make_unique<Whisky>(
                        std::make_unique<Espresso>())));

    client(std::move(cf));

    std::cout << "\n\n";

    CoffeeBuilder cb;
    cb.create_base<Espresso>().add<Whisky>().add<Whisky>().add<ExtraEspresso>();

    client(cb.get_coffee());
}
