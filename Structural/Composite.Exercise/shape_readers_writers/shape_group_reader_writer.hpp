#ifndef SHAPEGROUPREADERWRITER_HPP
#define SHAPEGROUPREADERWRITER_HPP

#include "../shape_factories.hpp"
#include "../shape_group.hpp"
#include "shape_reader_writer.hpp"

namespace Drawing
{
    namespace IO
    {
        class ShapeGroupReaderWriter : public ShapeReaderWriter
        {
            ShapeFactory& sf_;
            ShapeRWFactory& srwf_;

        public:
            ShapeGroupReaderWriter(ShapeFactory& sf, ShapeRWFactory& srwf)
                : sf_{sf}, srwf_{srwf}
            {}

            void read(Shape &shp, std::istream &in);
            void write(const Shape &shp, std::ostream &out);
        };
    }
}

#endif // SHAPEGROUPREADERWRITER_HPP
