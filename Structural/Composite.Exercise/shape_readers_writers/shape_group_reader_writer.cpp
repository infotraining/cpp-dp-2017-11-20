#include "shape_group_reader_writer.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

namespace
{
    bool is_registered =
            SingletonShapeRWFactory::instance()
                .register_creator(make_type_index<ShapeGroup>(),
                                  [] { return make_unique<ShapeGroupReaderWriter>(
                                        SingletonShapeFactory::instance(),
                                        SingletonShapeRWFactory::instance());
                                  });
}


void ShapeGroupReaderWriter::read(Shape &shp, istream &in)
{
    ShapeGroup& sg = static_cast<ShapeGroup&>(shp);

    int size;
    in >> size;

    for(int i = 0; i < size; ++i)
    {
        string id;
        in >> id;

        auto shp = sf_.create(id);
        auto shp_rw = srwf_.create(make_type_index(*shp));
        shp_rw->read(*shp, in);

        sg.add(move(shp));
    }
}

void ShapeGroupReaderWriter::write(const Shape &shp, ostream &out)
{
    const ShapeGroup& sg = static_cast<const ShapeGroup&>(shp);

    out << ShapeGroup::id << " " << sg.size();

    for(auto const& s : sg)
    {
        auto shp_rw = srwf_.create(make_type_index(*s));
        shp_rw->write(*s, out);
    }
}
