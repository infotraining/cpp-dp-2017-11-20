#include "bitmap.hpp"
#include <algorithm>
#include <array>
#include <deque>

using namespace std;

struct Bitmap::Implementation
{
    std::deque<char> image_;
};

Bitmap::~Bitmap() = default;

Bitmap::Bitmap(size_t size, char fill_char) :
    impl_(std::make_unique<Implementation>())
{
    impl_->image_.resize(size);
    fill_n(impl_->image_.begin(), size, fill_char);
}

void Bitmap::draw()
{
    cout << "Image: ";
    for (size_t i = 0; i < impl_->image_.size(); ++i)
        cout << impl_->image_[i];
    cout << endl;
}
