#ifndef SINGLETON_HPP
#define SINGLETON_HPP

#include <iostream>

template <typename T>
class SingletonHolder
{
private:
    SingletonHolder() = default;
    ~SingletonHolder() = default;

public:
    SingletonHolder(const SingletonHolder&) = delete;
    SingletonHolder& operator=(const SingletonHolder&) = delete;

    static T& instance()
    {
        static T unique_instance;

        return unique_instance;
    }
};

//class Logger
//{
//private:
//    Logger() = default;

//    friend class SingletonHolder<Logger>;
//};

//using SingletonLogger = SingletonHolder<Logger>;

//Logger& logger = SingletonLogger::instance();

#endif // SINGLETON_HPP
