#include <cassert>
#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

#include "rectangle.hpp"
#include "shape.hpp"
#include "shape_readers_writers/rectangle_reader_writer.hpp"
#include "shape_readers_writers/square_reader_writer.hpp"
#include "square.hpp"
#include <unordered_map>
#include <functional>
#include <typeindex>

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

//unique_ptr<Shape> create_shape(const string& id)
//{
//    if (id == Rectangle::id)
//        return make_unique<Rectangle>();
//    else if (id == Square::id)
//        return make_unique<Square>();

//    throw runtime_error("Unknown shape id");
//}

//unique_ptr<ShapeReaderWriter> create_shape_rw(Shape& shape)
//{
//    if (typeid(shape) == typeid(Rectangle))
//        return make_unique<RectangleReaderWriter>();
//    else if (typeid(shape) == typeid(Square))
//        return make_unique<SquareReaderWriter>();

//    throw runtime_error("Unknown shape id");
//}

namespace Draft
{
    template <
        typename Product,
        typename Key = std::string,
        typename Creator = std::function<std::unique_ptr<Product>()>
    >
    class GenericFactory
    {
        std::unordered_map<Key, Creator> creators_;
    public:
        bool register_creator(const Key& key, const Creator& creator)
        {
            return creators_.insert(std::make_pair(key, creator)).second;
        }

        std::unique_ptr<Product> create(const Key& key) const
        {
            auto& creator = creators_.at(key);
            return creator();
        }
    };
}

using ShapeFactory = Draft::GenericFactory<Shape>;
using ShapeRWFactory = Draft::GenericFactory<ShapeReaderWriter, std::type_index>;

template <typename T>
std::type_index make_type_index(const T& obj)
{
    return std::type_index(typeid(obj));
}

template <typename T>
std::type_index make_type_index()
{
    return std::type_index(typeid(T));
}

class GraphicsDoc
{
    vector<unique_ptr<Shape>> shapes_;
    ShapeFactory& sf_;
    ShapeRWFactory& srwf_;

public:
    GraphicsDoc(ShapeFactory& sf, ShapeRWFactory& srwf) : sf_{sf}, srwf_{srwf}
    {}

    void add(unique_ptr<Shape> shp)
    {
        shapes_.push_back(move(shp));
    }

    void render()
    {
        for (const auto& shp : shapes_)
            shp->draw();
    }

    void load(const string& filename)
    {
        ifstream file_in{filename};

        if (!file_in)
        {
            cout << "File not found!" << endl;
            exit(1);
        }

        while (file_in)
        {
            string shape_id;
            file_in >> shape_id;

            if (!file_in)
                return;

            cout << "Loading " << shape_id << "..." << endl;

            auto shape = sf_.create(shape_id);
            auto shape_rw = srwf_.create(make_type_index(*shape));

            shape_rw->read(*shape, file_in);

            shapes_.push_back(move(shape));
        }
    }

    void save(const string& filename)
    {
        ofstream file_out{filename};

        for (const auto& shp : shapes_)
        {            
            auto shape_rw = srwf_.create(make_type_index(*shp));
            shape_rw->write(*shp, file_out);
        }
    }
};

int main()
{   
    cout << "Start..." << endl;

    GraphicsDoc doc{SingletonShapeFactory::instance(), SingletonShapeRWFactory::instance()};

    doc.load("drawing.txt");

    cout << "\n";

    doc.render();

    doc.save("new_drawing.txt");
}
