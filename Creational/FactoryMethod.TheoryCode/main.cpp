#include <cstdlib>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <list>

#include "factory.hpp"

using namespace std;
namespace Canonical
{

class Service
{
    shared_ptr<LoggerCreator> creator_;

public:
    Service(shared_ptr<LoggerCreator> creator) : creator_(creator)
    {
    }

    Service(const Service&) = delete;
    Service& operator=(const Service&) = delete;

    void use()
    {
        auto logger = creator_->create_logger();
        logger->log("Client::use() has been started...");
        run();
        logger->log("Client::use() has finished...");
    }
protected:
    virtual void run() {}
};
}

class Service
{
    LoggerCreator logger_creator_;

public:
    Service(LoggerCreator creator) : logger_creator_(creator)
    {
    }

    Service(const Service&) = delete;
    Service& operator=(const Service&) = delete;

    void use()
    {
        ConsoleLogger logger;
        logger.log("log");

        auto logger = logger_creator_();
        logger->log("Client::use() has been started...");
        run();
        logger->log("Client::use() has finished...");
    }
protected:
    virtual void run() {}
};

using LoggerFactory = std::unordered_map<std::string, LoggerCreator> ;

int main()
{
    LoggerFactory logger_factory;
    logger_factory.insert(make_pair("ConsoleLogger",&make_unique<ConsoleLogger>));
    logger_factory.insert(make_pair("FileLogger", [] { return make_unique<FileLogger>("data.log"); }));
    logger_factory.insert(make_pair("DbLogger", [] { return make_unique<DbLogger>("localhost/db"); }));

    Service srv(logger_factory.at("DbLogger"));
    srv.use();
}

void most_popular_factory_method()
{
    list<int> vec = { 1, 2, 3 };

    for(const auto& item : vec)
    {
        cout << item << " ";
    }

    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        const auto& item = *it;

        cout << item << " ";
    }
}
